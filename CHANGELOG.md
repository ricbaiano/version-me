# Change Log

All notable changes to this project will be documented in this file. See [versionize](https://github.com/versionize/versionize) for commit guidelines.

<a name="2.9.5"></a>
## [2.9.5](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.5) (2023-2-22)

### ⚡ Continuous Integration

* **TESTEJJ-6:** Ajuste no jenkinsfile teste versao 4 ([c0da5f6](https://www.github.com/ricbaiano/version-me/commit/c0da5f6c43fab34a2088c74a09408e9b6d97f819))

<a name="2.9.4"></a>
## [2.9.4](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.4) (2023-2-22)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile teste versao 4 ([ef040ef](https://www.github.com/ricbaiano/version-me/commit/ef040efd4cb767a592affa745896310a7cd6c3a7))

<a name="2.9.3"></a>
## [2.9.3](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.3) (2023-2-22)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile teste versao 3 ([c5860d6](https://www.github.com/ricbaiano/version-me/commit/c5860d6b770f41c1950cb2fccc18ef1ed8c374af))

<a name="2.9.2"></a>
## [2.9.2](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.2) (2023-2-22)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile teste versao 2 ([f906907](https://www.github.com/ricbaiano/version-me/commit/f906907a02dcf112e72f5fb216591699a0eff639))

<a name="2.9.1"></a>
## [2.9.1](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.1) (2023-2-22)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile teste versao 1 ([0ce3ef7](https://www.github.com/ricbaiano/version-me/commit/0ce3ef72c6c0e259a52d1422a38841e8c75cdad9))

<a name="2.9.0"></a>
## [2.9.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.9.0) (2023-2-22)

### 🆕 Features

* SSCDD-XX - Nova feature ABCD ([ddaf87c](https://www.github.com/ricbaiano/version-me/commit/ddaf87cfc8a09e84698d0003cffad7fa10e1cf65))

<a name="2.8.0"></a>
## [2.8.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.8.0) (2023-2-17)

### 🆕 Features

* SSCDD-99 - Ajuste no jenkinsfile terceiro build ([0328a7b](https://www.github.com/ricbaiano/version-me/commit/0328a7bb422b5bfe6480b0e84df7004823996c6b))

<a name="2.7.0"></a>
## [2.7.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.7.0) (2023-2-17)

### 🆕 Features

* **TESTEJJ-6:** Nova tela 23 ([aba23fe](https://www.github.com/ricbaiano/version-me/commit/aba23fefe8be93745a87fe228f4d0f35b86ebba0))

<a name="2.6.0"></a>
## [2.6.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.6.0) (2023-2-17)

### 🆕 Features

* **TESTEJJ-5:** Nova tela 21 ([0b82d15](https://www.github.com/ricbaiano/version-me/commit/0b82d159b0234ff48279c72c64182a9970aba988))
* **TESTEJJ-6:** Nova tela 22 ([54885c2](https://www.github.com/ricbaiano/version-me/commit/54885c20cb6b613c430bebf473231b4bb3c1c5ec))

<a name="2.5.6"></a>
## [2.5.6](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.6) (2023-2-17)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile terceiro build ([4beec92](https://www.github.com/ricbaiano/version-me/commit/4beec9234ff1e35aafee6d3ff24b17f23970e380))

### Other

* ci:TESTEJJ-4 Ajuste no jenkinsfile terceiro build ([bdf2406](https://www.github.com/ricbaiano/version-me/commit/bdf2406ab9589e3a41a09088ff782c2664158aa2))

<a name="2.5.5"></a>
## [2.5.5](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.5) (2023-2-17)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile segundo build ([68cd8bf](https://www.github.com/ricbaiano/version-me/commit/68cd8bf22c1d573ea5fd3c82dbe0e70826d557f5))

<a name="2.5.4"></a>
## [2.5.4](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.4) (2023-2-17)

### ⚡ Continuous Integration

* Ajuste no jenkinsfile primeiro build ([f41d34b](https://www.github.com/ricbaiano/version-me/commit/f41d34b22648092e4f04535d6b443baf08fc597a))

<a name="2.5.3"></a>
## [2.5.3](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.3) (2023-2-17)

### ⚡ Continuous Integration

* Configurando jenkinsfile inicial ([a01de3a](https://www.github.com/ricbaiano/version-me/commit/a01de3a11a0eb7d66a278f4b97ad72a7a3aea142))
* Configurando jenkinsfile inicial ([74f91a3](https://www.github.com/ricbaiano/version-me/commit/74f91a3e57ebd2c138d2d34d67d30f6b8c25dc9a))
* TESTEJJ-1 testando deploy ([c1859b1](https://www.github.com/ricbaiano/version-me/commit/c1859b13f5ba587b7202f983e273dd4f118e7b19))

### 📝 Documentation

* atualizando o readme com jenkins ([3a38280](https://www.github.com/ricbaiano/version-me/commit/3a38280ae52c75d71ee31f53a62e990b50ef04cc))

### Other

* Merge branch 'main' of https://github.com/ricbaiano/version-me ([825955c](https://www.github.com/ricbaiano/version-me/commit/825955c5683b6adefa2ac62c317840a4bfa05b45))

<a name="2.5.2"></a>
## [2.5.2](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.2) (2023-2-13)

### 📝 Documentation

* Configurando o arquivo README.md do projeto ([b1df903](https://www.github.com/ricbaiano/version-me/commit/b1df903c84a89ac58b187e646b6905bfb0e534a7))

<a name="2.5.1"></a>
## [2.5.1](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.1) (2023-2-11)

### 📝 Documentation

* Ajustando documento de configuracao do versionamento e commit semantico ([3468909](https://www.github.com/ricbaiano/version-me/commit/3468909c487054792e3cfc15fac3d99512529bf1))

### ⚙️ Refactor

* refatorando codigo da tela principal do sistema ([24b0484](https://www.github.com/ricbaiano/version-me/commit/24b048423b79d46088c419f41bf22d9eb83d436e))

<a name="2.5.0"></a>
## [2.5.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.5.0) (2023-2-11)

### ✨ Features

* nova tela 13 no sistema ([5f46629](https://www.github.com/ricbaiano/version-me/commit/5f466299a1beecf750647036bf843976fab4ae99))

### 🐛 Bug Fixes

* corrigindo codigo da tela yzt ([ffc76b5](https://www.github.com/ricbaiano/version-me/commit/ffc76b542c216ddeb50e225d1e38c7e37c614b29))

### ⚡ Integracao Continua

* Ajustando configuracao do actions no github ([5425d7e](https://www.github.com/ricbaiano/version-me/commit/5425d7eedf3903e3ea6b61a9cc843be902e80652))

<a name="2.4.0"></a>
## [2.4.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.4.0) (2023-2-11)

### ✨ Features

* implementando nova tela xpto ([128f293](https://www.github.com/ricbaiano/version-me/commit/128f293393b4187f9248f295e9d2346ff0021539))

### Other

* corrigindo nova tela xpto ([7ba0616](https://www.github.com/ricbaiano/version-me/commit/7ba061650d130746c24a86a8c075096a00e57cd7))

<a name="2.3.2"></a>
## [2.3.2](https://www.github.com/ricbaiano/version-me/releases/tag/v2.3.2) (2023-2-11)

### 🚀 Integracao Continua

* Melhorando configuracao de integracao continua ([96a9e81](https://www.github.com/ricbaiano/version-me/commit/96a9e8145c0b84d59c7b04290a2ee2e2630f609b))

<a name="2.3.1"></a>
## [2.3.1](https://www.github.com/ricbaiano/version-me/releases/tag/v2.3.1) (2023-2-10)

<a name="2.3.0"></a>
## [2.3.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.3.0) (2023-2-10)

### Features

* ajustando tag no git ([06f95d9](https://www.github.com/ricbaiano/version-me/commit/06f95d98f3046708796c5780c09aa5ace151e519))

<a name="2.2.0"></a>
## [2.2.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.2.0) (2023-2-10)

### Features

* Nova tela de cadastro ([afe76b0](https://www.github.com/ricbaiano/version-me/commit/afe76b08c8ed519dcfaaaeff76b433f4658fdfb3))

<a name="2.1.0"></a>
## [2.1.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.1.0) (2023-2-10)

### Features

* Implementando alguma coisa ([996df56](https://www.github.com/ricbaiano/version-me/commit/996df5684214a9c331786a3aba13aa9c7c960565))
* Implementando CI github ([f20666c](https://www.github.com/ricbaiano/version-me/commit/f20666cc2bd9c4d88adbef536c0c9b49436bafa2))

<a name="2.0.0"></a>
## [2.0.0](https://www.github.com/ricbaiano/version-me/releases/tag/v2.0.0) (2023-2-10)

### Features

* Mudanca de comportamento ([af11713](https://www.github.com/ricbaiano/version-me/commit/af11713ee29e6988358139e53daf22bc0f54f45a))

### Breaking Changes

* Mudanca de comportamento ([af11713](https://www.github.com/ricbaiano/version-me/commit/af11713ee29e6988358139e53daf22bc0f54f45a))

<a name="1.2.0"></a>
## [1.2.0](https://www.github.com/ricbaiano/version-me/releases/tag/v1.2.0) (2023-2-10)

### Features

* Nova mensagem informando o ajuste ([33cca39](https://www.github.com/ricbaiano/version-me/commit/33cca39dbef3031b6cd7d73f559756fab3304830))
* Novo ajuste ([d98043a](https://www.github.com/ricbaiano/version-me/commit/d98043a7782753f1a1815ec4a1e5b4bed0bb1fa6))

### Bug Fixes

* Ajuste na quantidade de mensagens do sistema ([7b3d6f8](https://www.github.com/ricbaiano/version-me/commit/7b3d6f85a3ac7aa51e5992579cd8239f1777d3dc))

<a name="1.1.0"></a>
## [1.1.0](https://www.github.com/ricbaiano/version-me/releases/tag/v1.1.0) (2023-2-10)

### Features

* Inclusao de Mensagem 7 no sistema ([e266ea1](https://www.github.com/ricbaiano/version-me/commit/e266ea100207cf070e60503750647f70ae78a638))

<a name="1.0.3"></a>
## [1.0.3](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.3) (2023-2-10)

<a name="1.0.2"></a>
## [1.0.2](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.2) (2023-2-10)

<a name="1.0.1"></a>
## [1.0.1](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.1) (2023-2-10)

<a name="1.0.0"></a>
## [1.0.0](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.0) (2023-2-10)

### Features

* Nova Mensagem no console ([c1be259](https://www.github.com/ricbaiano/version-me/commit/c1be259c854fb8a03c498e03f97075b8368379ff))
* Nova Mensagem no console 2 ([5ebe9c0](https://www.github.com/ricbaiano/version-me/commit/5ebe9c0cc3b10a8d16c2163e96376402bdb5d96e))

<a name="1.0.0"></a>
## [1.0.0](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.0) (2023-2-10)

### Features

* Nova Mensagem no console ([c1be259](https://www.github.com/ricbaiano/version-me/commit/c1be259c854fb8a03c498e03f97075b8368379ff))

<a name="1.0.0"></a>
## [1.0.0](https://www.github.com/ricbaiano/version-me/releases/tag/v1.0.0) (2023-2-10)


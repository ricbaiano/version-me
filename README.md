﻿<h2 align='center'>SIAPI - Sistema Integrado de Acompanhamento da Propriedade Intelectual T</h2>

--------------------------------------------------------------------------
<br>
<p>
  <img src="https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white"/>
  <img src="https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white"/>
  <img src="https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E"/>
  <img src="https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white"/>
  <img src="https://img.shields.io/badge/Oracle-F80000?style=for-the-badge&logo=Oracle&logoColor=white"/>

</p>
<br>
<h3> Tópicos</h3>

🔹: [Descrição do projeto](#descrição-do-projeto)

🔹: [Funcionalidades](#funcionalidades)

🔹: [Pré-requisitos para instalação/desenvolvimento da aplicação](#Pré-requisitos-para-instalação/desenvolvimento-da-aplicação)

🔹: [Como rodar o banco de dados](#como-rodar-o-banco-de-dados)

🔹: [Como rodar a aplicação](#como-rodar-a-aplicação-arrow_forward)

## Descrição do Projeto
<p>
  Este projeto tem por objetivo ....
</p>

## Funcionalidades

✅ : 1

✅ : 2

✅ : 3

✅ : 4

✅ : ...

## Pré-requisitos para instalação/desenvolvimento da aplicação

⚠️ : [IIS em sistema operacional Windows 2012+]

⚠️ : [Oracle 11+ (Banco atual na versão Oracle 19)]

⚠️ : [Microsoft.NET Framework 4.5.1]

⚠️ : [TNSNAMES.ORA configurado para acessar os ambientes de DESENV, HOMOLOG e PROD do CENPES]

## Como rodar o banco de dados

1) 
2) 

OBS: Este script executará a criação de:

. Tabelas necessárias
. Sequences das tabelas

## Como rodar a aplicação

1) No terminal, clone o projeto:

> git@github.com:atiautomacao/xxxx



## Como rodar os testes

Para o backend, acesse a pasta "xxxx" e execute:
> npm test

Para o frontend, acesse a pasta "xxxx" e execute:
> 

## Lista de Endpoints

|     Rotas     |                  Função                  |
|:-------------:|:----------------------------------------:|
|    /users     |       Acessar cadastro de usuários       |



## Linguagens, dependencias e libs utilizadas 📚:
- [Angular](https://angular.io/)
- [Angular Material](https://material.angular.io/)
- [Node](https://nodejs.org/en/)
- [Express](https://expressjs.com/pt-br/)
- [PG-Promisse](https://www.npmjs.com/package/pg-promise)

## Tarefas em aberto
Features futuras:

📝: Tarefa 1

📝: Tarefa 2

## Desenvolvedores/Contribuintes 🔨:
Liste o time responsável pelo desenvolvimento do projeto

| Richard Barbosa | Issa Ubeid | Raphael Rubini | Vitor Guimaraes |
|:-------:|:-------:|:-------:|:-------:|

## Licença
The [MIT License]() (MIT)
